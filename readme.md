# Kafka

## Basic usage

### Start Kafka nodes

```bash
docker-compose up -d
```

### Create a topic

```bash
docker exec broker \
kafka-topics --bootstrap-server broker:9092 \
             --create \
             --topic quickstart
```

### Write messages to the topic

```bash
docker exec --interactive --tty broker \
kafka-console-producer --bootstrap-server broker:9092 \
                       --topic quickstart
 ```

### Read messages from the topic

```bash
docker exec --interactive --tty broker \
kafka-console-consumer --bootstrap-server broker:9092 \
                       --topic quickstart \
                       --from-beginning
```

___

## ksqlDB

### Run ksqlDB

```bash
docker compose up -d
```

### Start ksql cli

```bash
docker exec -it ksqldb-cli ksql http://ksqldb-server:8088
```

### Create stream

```bash
CREATE STREAM riderLocations (profileId VARCHAR, latitude DOUBLE, longitude DOUBLE)
  WITH (kafka_topic='locations', value_format='json', partitions=1);
```
