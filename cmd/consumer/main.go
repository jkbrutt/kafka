package main

import (
	"fmt"
	"os"

	"gopkg.in/confluentinc/confluent-kafka-go.v1/kafka"
)

func main() {
	c, err := kafka.NewConsumer(&kafka.ConfigMap{
		"bootstrap.servers": "localhost:9092",
		"group.id":          "test",
		"auto.offset.reset": "smallest",
	})

	if err != nil {
		fmt.Printf("Failed to start consumer: %s\n", err)
		os.Exit(1)
	}
	var run = true
	var topics = []string{"quickstart"}

	err = c.SubscribeTopics(topics, nil)
	if err != nil {
		fmt.Printf("Failed to subscribe topics: %s\n", err)
		os.Exit(1)
	}

	fmt.Println("start listening ...")
	for run {
		ev := c.Poll(100)
		switch e := ev.(type) {
		case *kafka.Message:
			fmt.Printf("%% Message on %s:\n%s\n",
				e.TopicPartition, string(e.Value))
		case kafka.PartitionEOF:
			fmt.Printf("%% Reached %v\n", e)
		case kafka.Error:
			fmt.Fprintf(os.Stderr, "%% Error: %v\n", e)
			run = false
		default:
			// fmt.Printf("Ignored %v\n", e)
		}
	}

	c.Close()
}
